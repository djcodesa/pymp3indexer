#!/usr/bin/env python
import eyeD3
import MySQLdb as mdb
import os, sys
from glob import glob
from pprint import pprint
from hurry.filesize import size, si
import md5, hashlib

class MakeIndex():
    """
    Class that creates an index of all my music and where its located on my LAN
    """
    def __init__(self):
        self.locations = ["/var/backups/mbhdd"]
        self.dbhost = 'localhost'
        self.dbuser = 'root'
        self.dbpass = 'W@nk3r0)'
        self.dbname = 'mp3index'
        self.conn   = None
        self.connect_to_mysql()
    
    def connect_to_mysql(self):
        try:
            self.conn = mdb.connect(self.dbhost, self.dbuser, self.dbpass, self.dbname)
        except Exception as e:
            self.error("Error connecting to Target MySQL Server", e)
    
    def mp3_tag_info(self, file_path):
        """
        Return a dictionary contining tag info from mp3 header.
        """
        try:
            tag = eyeD3.Tag()
            #Full mp3 file path.
            tag.link(file_path, eyeD3.ID3_ANY_VERSION) 
            return {"artist"   :  tag.getArtist(), "album"    :  tag.getAlbum(), "title"    :  tag.getTitle() }
        except Exception as e:
            pprint(e)
            
    def recurse_directory(self, start_dir):
        """
        Recurse through a directory, store all the filenames in a text file.
        """
        files = []
        directories = [start_dir]
        while len(directories) > 0:
            directory = directories.pop()
            for name in os.listdir(directory):
                fullpath = os.path.join(directory,name)
                if os.path.isfile(fullpath):
                    filename, ext = os.path.splitext(fullpath)
                    if(ext == ".mp3"):
                        files.append(fullpath)
                elif os.path.isdir(fullpath):
                    directories.append(fullpath)
        return files
    
    def md5_for_file(self, f, block_size=2**20):
        m = hashlib.md5()
        while True:
            data = f.read(10240)
            if len(data) == 0:
                break
            m.update(data)
        return m.hexdigest()
    
    def run_scan(self):
        """
        Run a globish scan for files, get the info and populate the database.
        """
        for location in self.locations:
            if(os.path.exists(location)):
                files = self.recurse_directory(location)
                cur = self.conn.cursor(mdb.cursors.DictCursor)
                for path in files:
                    
                    filename = os.path.basename(path)
                    just_path = str(os.path.realpath(path)).replace(filename,"")
                    info = self.mp3_tag_info(path)
                    #filesize = size(os.stat(path), system=si)
                    filesize = os.path.getsize(path)
                    filesize = 1
                    fh = open(path)
                    checksum = str(self.md5_for_file(fh))
                    
                    pprint(checksum)
                    
                    try:
                        sql ="""
                        INSERT INTO `music` (filename, directory, size, checksum, artist, album, title) 
                        VALUES (%s, %s, %s, %s, %s, %s, %s )
                        """
                        cur.execute(sql, [filename, just_path, filesize, checksum, info['artist'], info['album'], info['title']] )
                        self.conn.commit()  
                       
                    except mdb.Error, e:
                        pprint(e)
                    except Exception as e:
                        pprint(e)
                cur.close()
                         
    def close_connection(self):
        self.conn.close()
                    

if __name__ == "__main__":
    mk = MakeIndex()
    mk.run_scan()
    mk.close_connection()
